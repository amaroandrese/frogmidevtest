Implementación de Api del Transantiago para obtener recorridos y paraderos en la ciudad de Santiago de Chile, el proyecto fue realizado con Android Studio en Kotlin utilizando MVP

La aplicación debe tener las siguientes funciones :

- Uso de Fragments para ambas vistas (lista de paraderos, listado de recorridos por paradero)
- Paginación de 20 elementos.
- Persistir Paraderos y Recorridos para funcionar de forma offline.
- Para el listado de paraderos, basta con mostrar el nombre del paradero y su código.
- Para el listado de recorridos, basta con mostrar el nombre del recorrido y el código del bus.

La aplicación realiza todas las funciones descritas arriba, excepto la paginación de paraderos cuando está en modo offline y paginación de paraderos, ésto por decisión del desarrollador.
