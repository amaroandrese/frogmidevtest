package com.example.frogmidevtest

import android.content.Context
import androidx.multidex.MultiDex
import io.realm.Realm

/**
 *
 * Class: Application class to instance the Realm Database.
 *
 * */

class Application: android.app.Application() {

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        Realm.init(this) //init realmdb this covers all use of realm    within the project.
    }



}