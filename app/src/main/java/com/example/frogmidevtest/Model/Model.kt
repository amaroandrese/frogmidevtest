package com.example.frogmidevtest.Model

import com.example.frogmidevtest.Dao.*
import com.example.frogmidevtest.Dao.RealmObject.Results
import com.example.frogmidevtest.Dao.RealmObject.Route
import com.example.frogmidevtest.Dao.RealmObject.Stop
import com.example.frogmidevtest.Interfaces
import com.example.frogmidevtest.Model.DataModel.RouteDataModel
import com.example.frogmidevtest.Volley.OperationCallback
import com.example.frogmidevtest.Volley.VolleyImplementation
import io.realm.Realm


/**
 * Model: Class ni charge of the communication with the VolleyImplementation, also insert the data in Realm when arrives from the API.
 * */
class Model(var mPresenter: Interfaces.RequiredPresenterMethods): Interfaces.ProvidedModelMethods {
    val realm: Realm by lazy {
        Realm.getDefaultInstance()
    }

    override fun getStopsData(pageNumber:Int,onFinishListener: Interfaces.OnFinishListener) {

        //online implementation
        VolleyImplementation.getInstance(mPresenter.getAppContext()).getAllStops(-70.653674,-33.444087,5000,pageNumber,object:OperationCallback {
            override fun onStopSuccess(mList: ArrayList<Results>, stop: Stop) {
                //insert a stop in Realm.
                realm.stopDao().addStop(stop)
                onFinishListener.onResultStopSuccess(mList)
            }

            override fun onAddedStopSuccess(mList: ArrayList<Results>, stop: Stop) {/*NOT APPLY*/}

            override fun onRouteSuccess(mList: ArrayList<RouteDataModel>,mDBList: ArrayList<Route>) {/*NOT APPLY*/}
            override fun onError(obj: Any?) {
                onFinishListener.onResultFail(obj.toString())
            }
        })
    }

    override fun getRoutesData(stop_id:String, onFinishListener: Interfaces.OnFinishListener) {
        //online implementation
        VolleyImplementation.getInstance(mPresenter.getAppContext()).getAllRoutes(stop_id,object:OperationCallback {
            override fun onStopSuccess(mList: ArrayList<Results>, stop: Stop) {//NOT APPLY
            }

            override fun onAddedStopSuccess(mList: ArrayList<Results>, stop: Stop) {
                TODO("Not yet implemented")
            }

            override fun onRouteSuccess(mList: ArrayList<RouteDataModel>,mDBList: ArrayList<Route>) {
                for (mRoute in mDBList){ ////insert a routes in Realm.
                    realm.routeDao().addRoute(mRoute)
                }
                onFinishListener.onResultRoutesSuccess(mList)
            }
            override fun onError(obj: Any?) {
                onFinishListener.onResultFail(obj.toString())
            }
        })
    }

    override fun getAddedStopsData(pageNumber:Int,onFinishListener: Interfaces.OnFinishListener) {

        //online implementation
        VolleyImplementation.getInstance(mPresenter.getAppContext()).getAddedStops(-70.653674,-33.444087,5000,pageNumber,object:OperationCallback {
            override fun onStopSuccess(mList: ArrayList<Results>, stop: Stop) {/*NOT APPLY*/}
            override fun onAddedStopSuccess(mList: ArrayList<Results>, stop: Stop) {
                //insert a stop in Realm.
                realm.stopDao().addStop(stop)
                onFinishListener.onResultAddedStopSuccess(mList)
            }
            override fun onRouteSuccess(mList: ArrayList<RouteDataModel>,mDBList: ArrayList<Route>) {/*NOT APPLY*/}
            override fun onError(obj: Any?) {
                onFinishListener.onResultFail(obj.toString())
            }
        })
    }
}