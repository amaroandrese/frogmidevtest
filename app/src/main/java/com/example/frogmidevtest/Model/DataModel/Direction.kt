package com.example.frogmidevtest.Model.DataModel


/**
 * Direction: Class that is use to save a direction Object from the API Response.
 * Has this variables names to match the API response parameters.
 * */

class Direction {

    constructor(direction_id: String, direction_headsign: String, direction_name: String,route_id:String) {
        this.direction_id = direction_id
        this.direction_headsign = direction_headsign
        this.direction_name = direction_name
        this.route_id = route_id
    }

    var direction_id: String
        get() {
            return field
        }
        set(value) {
            field = value}
    var direction_headsign:String
        get() {
            return field
        }
        set(value) {
            field = value
        }
    var direction_name:String
        get() {
            return field
        }
        set(value) {
            field = value
        }
    var route_id:String
        get() {
            return field
        }
        set(value) {
            field = value
        }

}