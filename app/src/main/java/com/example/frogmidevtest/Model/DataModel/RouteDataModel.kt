package com.example.frogmidevtest.Model.DataModel

import com.example.frogmidevtest.Model.DataModel.Direction
import org.json.JSONObject


/**
 *
 * Class RouteDataModel: A class that is use for the View in RouteFragment to have a Route Object.
 * Has this variables names to match the fields from the API Response.
 * */
class RouteDataModel {

    constructor(
        route_long_name: String,
        route_type: String,
        route_text_color: String,
        agency_id: String,
        route_id: String,
        route_color: String,
        direction: Direction
    ) {
        this.route_long_name = route_long_name
        this.route_type = route_type
        this.route_text_color = route_text_color
        this.agency_id = agency_id
        this.route_id = route_id
        this.route_color = route_color
        this.direction = direction
    }

    constructor(mJSONObject: JSONObject?){
        this.route_long_name = mJSONObject!!.getJSONObject("route").getString("route_long_name")
        this.route_type = mJSONObject.getJSONObject("route").getString("route_type")
        this.route_text_color = mJSONObject.getJSONObject("route").getString("route_text_color")
        this.agency_id = mJSONObject.getJSONObject("route").getString("agency_id")
        this.route_id = mJSONObject.getJSONObject("route").getString("route_id")
        this.route_color = mJSONObject.getJSONObject("route").getString("route_color")
        this.direction = Direction(
            mJSONObject.getJSONObject("direction").getString("direction_id"),
                    mJSONObject.getJSONObject("direction").getString("direction_headsign"),
                    mJSONObject.getJSONObject("direction").getString("direction_name"),
                    mJSONObject.getJSONObject("direction").getString("route_id")
        )
    }

    var route_long_name:String
        get() {
            return field
        }
        set(value) {field = value}
    var route_type:String
        get() {
            return field
        }
        set(value) {field = value}
    var route_text_color:String
        get() {
            return field
        }
        set(value) {field = value}
    var agency_id:String
        get() {
            return field
        }
        set(value) {field = value}
    var route_id:String
        get() {
            return field
        }
        set(value) {field = value}
    var route_color:String
        get() {
            return field
        }
        set(value) {field = value}
    var direction:Direction
        get() {
            return field
        }
        set(value) {field = value}
}