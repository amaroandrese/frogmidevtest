package com.example.frogmidevtest

import android.content.Context
import com.example.frogmidevtest.Dao.RealmObject.Results
import com.example.frogmidevtest.Dao.RealmObject.Route
import com.example.frogmidevtest.Dao.RealmObject.Stop
import com.example.frogmidevtest.Model.DataModel.RouteDataModel
import com.example.frogmidevtest.View.RouteFragment
import com.example.frogmidevtest.View.StopFragment
import com.example.frogmidevtest.ViewModel.RouteViewModel
import com.example.frogmidevtest.ViewModel.StopViewModel
import io.realm.RealmResults

interface Interfaces {

    /**
     * View Interfaces
     * */

    //Operations available from the View to the Presenter
    interface RequiredViewMethods{

        fun getAppContext(): Context
        fun getActivityContext() : Context
        fun notifyStopsChanged(stopList: ArrayList<Results>)
        fun notifyRouteChanged(routeList: ArrayList<RouteDataModel>)
        fun notifyAddedChanged(stopList: ArrayList<Results>)
    }


    /**
     * Presenter Interfaces
     * */

    //Operations provided to the View from the Presenter
    interface ProvidedPresenterMethods{

        fun updateStopsValues(pageNumber: Int)
        fun updateRoutesValues(stop_id:String)
        fun addStopsValues(pageNumber:Int)

    }

    //Operations required from the Model for the Presenter
    interface RequiredPresenterMethods{
        fun getAppContext():Context
        fun getActivityContext():Context
    }

    /**
     * Model Interfaces
     * */

    //Operations available from Model to Presenter
    interface ProvidedModelMethods{

        fun getStopsData(pageNumber:Int,onFinishListener: OnFinishListener)
        fun getRoutesData(stop_id:String,onFinishListener: OnFinishListener)
        fun getAddedStopsData(pageNumber: Int, onFinishListener: OnFinishListener)
    }


    interface OnFinishListener {
        fun onResultStopSuccess(mStopsList: ArrayList<Results>)
        fun onResultAddedStopSuccess(mStopsList: ArrayList<Results>)
        fun onResultRoutesSuccess(mRoutesList: ArrayList<RouteDataModel>)
        fun onResultFail(mError: String)
    }


    /**
     *
     *      Offline interfaces: Interfaces for the app to work offline getting data from RealmDatabase
     * **/

    interface BasePresenter{
        fun start()
    }
    interface BaseView<T> {
        var mOfflinePresenter: T
        fun onError(errorMessage: Int)
        fun onDataLoading()
    }


    /**
    *       Stop Interfaces
    * */
    interface StopContract {

        interface View : BaseView<Presenter> {
            fun onStopLoaded(realmResults: RealmResults<Stop>?)
            fun getViewModel(): StopViewModel
        }

        interface Presenter : BasePresenter {
            fun loadStop(activity: StopFragment, page_number: Int)
        }
    }

    /**
     * Route Interfaces
     * */
    interface RouteContract {

        interface View : BaseView<Presenter> {
            fun onRouteLoaded(realmResults: RealmResults<Route>?)
            fun getViewModel(): RouteViewModel
        }

        interface Presenter : BasePresenter {
            fun loadRoutes(activity: RouteFragment, stop_id: String)
        }
    }






}