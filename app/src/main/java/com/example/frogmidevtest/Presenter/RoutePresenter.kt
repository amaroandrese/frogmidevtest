package com.example.frogmidevtest.Presenter

import androidx.lifecycle.Observer
import com.example.frogmidevtest.Dao.RealmObject.Route
import com.example.frogmidevtest.Interfaces
import com.example.frogmidevtest.View.RouteFragment
import io.realm.RealmResults

/**
 * RoutePresenter: Offline Presenter for RouteFragment to communicate with Realm.
 * */
class RoutePresenter(val view: Interfaces.RouteContract.View): Interfaces.RouteContract.Presenter {
    init {
        view.mOfflinePresenter = this
    }
    protected val viewModel by lazy {
        view.getViewModel()
    }
    override fun loadRoutes(activity: RouteFragment, stop_id: String) {
        viewModel.getRouteData(stop_id).observe(activity, Observer<RealmResults<Route>> { t ->
            view.onRouteLoaded(t)
        })
    }

    override fun start() {
    }


}