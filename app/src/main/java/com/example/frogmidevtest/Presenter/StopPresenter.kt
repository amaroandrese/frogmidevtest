package com.example.frogmidevtest.Presenter

import androidx.lifecycle.Observer
import com.example.frogmidevtest.Dao.RealmObject.Stop
import com.example.frogmidevtest.Interfaces
import com.example.frogmidevtest.View.StopFragment
import io.realm.RealmResults

/**
 * StopPresenter: Offline Presenter for StopFragment to communicate with Realm.
 * */
class StopPresenter(val view:Interfaces.StopContract.View): Interfaces.StopContract.Presenter{
    init {
        view.mOfflinePresenter = this
    }
    protected val viewModel by lazy {
        view.getViewModel()
    }
    override fun loadStop(activity: StopFragment, page_number:Int) {
        viewModel.getStopData(page_number).observe(activity, Observer<RealmResults<Stop>> { t ->
            view.onStopLoaded(t)
        })
    }

    override fun start() {
    }

}