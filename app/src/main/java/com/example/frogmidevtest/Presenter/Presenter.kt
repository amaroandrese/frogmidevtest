package com.example.frogmidevtest.Presenter

import android.content.Context
import android.util.Log
import com.example.frogmidevtest.Dao.RealmObject.Results
import com.example.frogmidevtest.Interfaces
import com.example.frogmidevtest.Model.DataModel.RouteDataModel
import java.lang.ref.WeakReference

/**
 * Presenter: This Presenter is in charge of all the communication between the Views and the Model when comes to onlineMode.
 * */

class Presenter: Interfaces.ProvidedPresenterMethods,Interfaces.RequiredPresenterMethods,Interfaces.OnFinishListener{

    var mView : WeakReference<Interfaces.RequiredViewMethods>
    lateinit var mModel: Interfaces.ProvidedModelMethods

    constructor(mView: Interfaces.RequiredViewMethods) {
        this.mView = WeakReference(mView)
    }

    private fun getView() : Interfaces.RequiredViewMethods? {
        return mView.get()
    }

    fun setModel(mModel: Interfaces.ProvidedModelMethods){
        this.mModel = mModel
    }

    override fun getAppContext(): Context {
        return getView()!!.getAppContext()
    }

    override fun getActivityContext(): Context {
        return getView()!!.getActivityContext()
    }
    override fun onResultStopSuccess(mStopsList: ArrayList<Results>) {
        getView()!!.notifyStopsChanged(mStopsList)
    }

    override fun addStopsValues(pageNumber:Int) {
        mModel.getAddedStopsData(pageNumber,this)
    }

    override fun onResultRoutesSuccess(mRoutesList: ArrayList<RouteDataModel>) {
        getView()!!.notifyRouteChanged(mRoutesList)
    }

    override fun onResultFail(mError: String) {
        Log.e("onResultFail", "Error de comunicación con la API: $mError")
    }

    override fun updateStopsValues(pageNumber: Int) {
        mModel.getStopsData(pageNumber, this)
    }

    override fun updateRoutesValues(stop_id:String) {
        mModel.getRoutesData(stop_id,this)
    }

    override fun onResultAddedStopSuccess(mStopsList: ArrayList<Results>) {
        getView()!!.notifyAddedChanged(mStopsList)
    }


}