package com.example.frogmidevtest.Dao.RealmObject

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.Index

/**
 * Realm Database Object for Route
 * */
open class Route (@SerializedName(value = "stop_id") var stop_id:String,
                  @SerializedName(value = "route_long_name") var route_long_name:String,
                  @SerializedName(value = "route_type") var route_type:String,
                  @SerializedName(value = "route_text_color") var route_text_color:String,
                  @SerializedName(value = "route_color") var route_color:String,
                  @SerializedName(value = "route_id") var route_id:String,
                  @SerializedName(value = "direction") var direction:String,
                  @SerializedName(value = "agency_id") var agency_id:String) : RealmObject(){
    constructor(): this("","","","","","","","")}
