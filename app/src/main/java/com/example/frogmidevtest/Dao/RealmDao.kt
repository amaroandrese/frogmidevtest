package com.example.frogmidevtest.Dao

import io.realm.Realm
import io.realm.RealmModel
import io.realm.RealmResults

/**
 * functions to accomplish an easy access to the Realm Object
 * */
fun <T: RealmModel> RealmResults<T>.asLiveData() = RealmLiveData<T>(this)
fun Realm.stopDao() : StopDao = StopDao(this)
fun Realm.routeDao() : RouteDao = RouteDao(this)

