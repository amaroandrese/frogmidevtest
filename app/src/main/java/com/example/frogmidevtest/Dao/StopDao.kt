package com.example.frogmidevtest.Dao

import androidx.lifecycle.LiveData
import com.example.frogmidevtest.Dao.RealmObject.Stop
import io.realm.Realm
import io.realm.RealmResults


/**
 * StopDao: Manage the queries to the RealmDatabase related to Stops
 * */
class StopDao(val realm: Realm) {

    fun addStop(mStop: Stop){
        realm.executeTransactionAsync{
            val stop = Stop()
            stop.has_next = mStop.has_next
            stop.page_number = mStop.page_number
            stop.total_results = mStop.total_results
            stop.total_pages = mStop.total_pages
            stop.results = mStop.results
            stop.page_size = mStop.page_size
            it.insert(stop)
        }
    }

    fun getAllStop(page_number:Int): LiveData<RealmResults<Stop>> {
        return realm.where(Stop::class.java).equalTo("page_number",page_number).findAllAsync().asLiveData()
    }
}