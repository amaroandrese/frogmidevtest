package com.example.frogmidevtest.Dao.RealmObject

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject


/**
 * Realm Database Object for a single Stop
 * */
open class Results(@SerializedName(value = "stop_lat") var stop_lat:String,
                   @SerializedName(value = "stop_code") var stop_code:String,
                   @SerializedName(value = "stop_lon") var stop_lon:String,
                   @SerializedName(value = "agency_id") var agency_id:String,
                   @SerializedName(value = "stop_id") var stop_id:String,
                   @SerializedName(value = "stop_name") var stop_name:String) : RealmObject(){
    constructor() : this("","","","","","")

}



