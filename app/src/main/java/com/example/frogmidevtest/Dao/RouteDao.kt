package com.example.frogmidevtest.Dao

import androidx.lifecycle.LiveData
import com.example.frogmidevtest.Dao.RealmObject.Route
import io.realm.Realm
import io.realm.RealmResults


/**
 * RouteDao: Manage the queries to the RealmDatabase realted to Routes
 * */

class RouteDao(val realm: Realm) {

    fun addRoute(mRoute: Route) {
        realm.executeTransactionAsync {
            val route = Route()
            route.stop_id = mRoute.stop_id
            route.route_long_name = mRoute.route_long_name
            route.route_type = mRoute.route_type
            route.route_text_color = mRoute.route_text_color
            route.route_color = mRoute.route_color
            route.route_id = mRoute.route_id
            route.direction = mRoute.direction
            route.agency_id = mRoute.agency_id
            it.insert(route)
        }
    }

    fun getAllRoutes(stop_id: String): LiveData<RealmResults<Route>> {
        return realm.where(Route::class.java).equalTo("stop_id", stop_id).distinct("stop_id","route_id","direction").findAllAsync()
            .asLiveData()
    }
}