package com.example.frogmidevtest.Dao.RealmObject

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject


/**
 *
 *
 * */
open class Directions(@SerializedName(value = "direction_id") var direction_id:String,
                      @SerializedName(value = "route_id") var route_id:String,
                      @SerializedName(value = "direction_headsign") var direction_headsign:String,
                      @SerializedName(value = "direction_name") var direction_name:String) : RealmObject(){
    constructor(): this("","","","")}