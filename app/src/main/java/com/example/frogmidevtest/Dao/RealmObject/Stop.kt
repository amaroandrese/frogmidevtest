package com.example.frogmidevtest.Dao.RealmObject

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject


/**
 * StopFull API response
 * */
open class Stop(@SerializedName(value = "has_next") var has_next:String,
                @SerializedName(value = "page_number") var page_number:Int,
                @SerializedName(value = "total_results") var total_results:Int,
                @SerializedName(value = "total_pages") var total_pages:Int,
                @SerializedName(value = "results") var results:String,
                @SerializedName(value = "page_size") var page_size:Int): RealmObject() {
    constructor(): this("",0,0,0, "",0)
}


