package com.example.frogmidevtest.Volley

import com.example.frogmidevtest.Dao.RealmObject.Results
import com.example.frogmidevtest.Dao.RealmObject.Route
import com.example.frogmidevtest.Dao.RealmObject.Stop
import com.example.frogmidevtest.Model.DataModel.RouteDataModel

interface OperationCallback {

    fun onStopSuccess(mList:ArrayList<Results>, stop: Stop)
    fun onAddedStopSuccess(mList:ArrayList<Results>, stop: Stop)
    fun onRouteSuccess(mList:ArrayList<RouteDataModel>,mDBList:ArrayList<Route>)
    fun onError(obj:Any?)
}