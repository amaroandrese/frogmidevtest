package com.example.frogmidevtest.Volley

import android.content.Context
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.frogmidevtest.Dao.RealmObject.Results
import com.example.frogmidevtest.Dao.RealmObject.Route
import com.example.frogmidevtest.Dao.RealmObject.Stop
import com.example.frogmidevtest.Model.DataModel.RouteDataModel
import org.json.JSONObject
import kotlin.collections.ArrayList

/**
 *
 * Class : VolleyImplementation
 * @param context : The context of the App
 *
 * */

class VolleyImplementation(val context: Context) {

    val TAG = "VolleyImplementation"
    private val requestQueue = Volley.newRequestQueue(context)
    private val mURLBase = "https://api.scltrans.it"

    /**
     *  fun getAllStops: Get 20 Stops from the coordinates in a radius.
     *  @param lon: Longitude
     *  @param lat: Latitude
     *  @param radius: Radius in meters
     *  @param pageNumber: NUmber of the pageResult that we get
     * */
    fun getAllStops(lon:Double,lat:Double,radius:Int,pageNumber:Int,callback:OperationCallback){

        val mUrl = "$mURLBase/v1/stops?limit=20&center_lon=$lon&center_lat=$lat&radius=$radius&page=$pageNumber"

        val mJsonObjectRequest = JsonObjectRequest(Request.Method.GET,mUrl,JSONObject(), Response.Listener { response ->

            Log.d(TAG,"getAllStops Query Response Achieved--")

            val mStopList = ArrayList<Results>()
            val mJSONResponse = JSONObject(response.toString())
            val mStops = mJSONResponse.optJSONArray("results")!!
            var mStopDataModel: Results
            for (i in 0 until mStops.length()){
                val mStop = mStops.getJSONObject(i)
                mStopDataModel =
                    Results(
                        mStop.getString("stop_lat"),
                        mStop.getString("stop_code"),
                        mStop.getString("stop_lon"),
                        mStop.getString("agency_id"),
                        mStop.getString("stop_id"),
                        mStop.getString("stop_name")
                    )
                mStopList.add(mStopDataModel)
            }

            val mDBStop = Stop(
                mJSONResponse.getString("has_next"),
                mJSONResponse.getInt("page_number"),
                mJSONResponse.getInt("total_results"),
                mJSONResponse.getInt("total_pages"),
                mJSONResponse.getString("results"),
                mJSONResponse.getInt("page_size")
            )

            callback.onStopSuccess(mStopList,mDBStop)

        },Response.ErrorListener { error ->
            val messageError= "error :".plus("message ${error.message}")
            callback.onError(messageError)
        })
        requestQueue.add(mJsonObjectRequest)

    }

    /**
     *  fun getAllRoutes: Get all routes according to an stop_id value.
     *  @param stop_id: Stops ID to get Routes
     * */
    fun getAllRoutes(stop_id:String,callback: OperationCallback){

        val mUrl = "$mURLBase/v3/stops/$stop_id/stop_routes"
        val mJsonObjectRequest = JsonObjectRequest(Request.Method.GET,mUrl,JSONObject(), Response.Listener { response ->

            Log.d(TAG,"getAllRoutes Query Response Achieved--")

            val mRouteList = ArrayList<RouteDataModel>()
            val mRouteDBList = ArrayList<Route>()
            val mJSONResponse = JSONObject(response.toString())
            val mRoutesJSONArray = mJSONResponse.optJSONArray("results")!!
            var mRouteDataModel: RouteDataModel
            for (i in 0 until mRoutesJSONArray.length()){
                val mRoute = mRoutesJSONArray.getJSONObject(i)
                mRouteDataModel = RouteDataModel(mRoute)
                mRouteList.add(mRouteDataModel)
            }
            for (i in 0 until mRoutesJSONArray.length()){
                val mRoute = mRoutesJSONArray.getJSONObject(i)
                val mRouteData = Route(
                    stop_id,
                    mRoute.getJSONObject("route").getString("route_long_name"),
                    mRoute.getJSONObject("route").getString("route_type"),
                    mRoute.getJSONObject("route").getString("route_text_color"),
                    mRoute.getJSONObject("route").getString("route_color"),
                    mRoute.getJSONObject("route").getString("route_id"),
                    mRoute.getJSONObject("direction").toString(),
                    mRoute.getJSONObject("route").getString("agency_id")
                )
                mRouteDBList.add(mRouteData)
            }

            callback.onRouteSuccess(mRouteList,mRouteDBList)

        },Response.ErrorListener { error ->
            val messageError= "error :".plus("message ${error.message}")
            callback.onError(messageError)
        })
        requestQueue.add(mJsonObjectRequest)
    }

    /**
     *  fun getAllStops: Get 20 Stops from the coordinates in a radius according to page_number, for Pagination.
     *  @param lon: Longitude
     *  @param lat: Latitude
     *  @param radius: Radius in meters
     *  @param pageNumber: Number of the pageResult that we get.
     * */
    fun getAddedStops(lon:Double,lat:Double,radius:Int,pageNumber:Int,callback:OperationCallback){

        val mUrl = "$mURLBase/v1/stops?limit=20&center_lon=$lon&center_lat=$lat&radius=$radius&page=$pageNumber"

        val mJsonObjectRequest = JsonObjectRequest(Request.Method.GET,mUrl,JSONObject(), Response.Listener { response ->

            Log.d(TAG,"getAddedStops Query Response Achieved--")
            val mStopList = ArrayList<Results>()
            val mJSONResponse = JSONObject(response.toString())
            val mStops = mJSONResponse.optJSONArray("results")!!
            var mStopDataModel: Results
            for (i in 0 until mStops.length()){
                val mStop = mStops.getJSONObject(i)
                mStopDataModel =
                    Results(
                        mStop.getString("stop_lat"),
                        mStop.getString("stop_code"),
                        mStop.getString("stop_lon"),
                        mStop.getString("agency_id"),
                        mStop.getString("stop_id"),
                        mStop.getString("stop_name")
                    )

                mStopList.add(mStopDataModel)
            }

            val mStop = Stop(
                mJSONResponse.getString("has_next"),
                mJSONResponse.getInt("page_number"),
                mJSONResponse.getInt("total_results"),
                mJSONResponse.getInt("total_pages"),
                mJSONResponse.getString("results"),
                mJSONResponse.getInt("page_size")
            )
            callback.onAddedStopSuccess(mStopList,mStop)

        },Response.ErrorListener { error ->
            val messageError= "error : ".plus("message ${error.message}")
            callback.onError(messageError)
        })
        requestQueue.add(mJsonObjectRequest)

    }
    companion object {
        private var INSTANCE: VolleyImplementation? = null
        @JvmStatic fun getInstance(context: Context): VolleyImplementation {
            return INSTANCE ?: VolleyImplementation(context)
                .apply { INSTANCE = this }
        }
        @JvmStatic fun destroyInstance() {
            INSTANCE = null
        }
    }
}