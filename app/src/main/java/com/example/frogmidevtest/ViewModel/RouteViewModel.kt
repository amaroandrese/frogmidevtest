package com.example.frogmidevtest.ViewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.frogmidevtest.Dao.RealmObject.Route
import com.example.frogmidevtest.Dao.routeDao
import io.realm.Realm
import io.realm.RealmResults

/**
 * Class that extend ViewModel that contains the methods related to Routes for the Presenter
 * */

open class RouteViewModel : ViewModel() {

    val realm: Realm by lazy {
        Realm.getDefaultInstance()
    }

    fun getRouteData(stop_id:String): LiveData<RealmResults<Route>> {
        return realm.routeDao().getAllRoutes(stop_id)
    }

    override fun onCleared() {
        realm.close()
        super.onCleared()
    }



}