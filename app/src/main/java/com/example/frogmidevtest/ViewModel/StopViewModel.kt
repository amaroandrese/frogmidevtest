package com.example.frogmidevtest.ViewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.frogmidevtest.Dao.RealmObject.Stop
import com.example.frogmidevtest.Dao.stopDao
import io.realm.Realm
import io.realm.RealmResults

/**
 * Class that extend ViewModel that contains the methods related to Stops for the Presenter
 * */
open class StopViewModel : ViewModel() {

    val realm: Realm by lazy {
        Realm.getDefaultInstance()
    }

    fun getStopData(page_number:Int): LiveData<RealmResults<Stop>> {
        return realm.stopDao().getAllStop(page_number)
    }

    override fun onCleared() {
        realm.close()
        super.onCleared()
    }

}