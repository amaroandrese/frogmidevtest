package com.example.frogmidevtest.View.Adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.frogmidevtest.Model.DataModel.RouteDataModel
import com.example.frogmidevtest.R

/**
 * RouteLisAdapter: Is the Adapter of the RouteFragment RecyclerView
 * @param items: List of Routes
 * */
class RouteListAdapter(val items:List<RouteDataModel>) : RecyclerView.Adapter<RouteViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RouteViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.recycler_item_route,parent,false)
        return  RouteViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RouteViewHolder, position: Int) = holder.bind(items[position])




}