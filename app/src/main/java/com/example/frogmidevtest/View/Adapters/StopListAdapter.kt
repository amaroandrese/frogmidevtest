package com.example.frogmidevtest.View.Adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.frogmidevtest.Dao.RealmObject.Results
import com.example.frogmidevtest.R


/**
 * StopListAdapter: Is the Adapter of the StopFragmet RecyclerView
 * @param items: List of Stops
 * @param clickListener: ItemClickListener to know when the user click an Stop.
 * */

class StopListAdapter(val items:List<Results>, val clickListener: ItemClickListener) : RecyclerView.Adapter<StopViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StopViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.recycler_item_stop,parent,false)
        return  StopViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: StopViewHolder, position: Int)  = holder.bind(items[position],clickListener)

    interface ItemClickListener {
        fun onCLick(mStopDataModel: Results)
    }
}