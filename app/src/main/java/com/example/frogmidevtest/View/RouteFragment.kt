package com.example.frogmidevtest.View

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.frogmidevtest.Dao.RealmObject.Results
import com.example.frogmidevtest.Dao.RealmObject.Route
import com.example.frogmidevtest.ViewModel.RouteViewModel
import com.example.frogmidevtest.Interfaces
import com.example.frogmidevtest.Model.DataModel.Direction
import com.example.frogmidevtest.Model.DataModel.RouteDataModel
import com.example.frogmidevtest.Model.Model
import com.example.frogmidevtest.Presenter.Presenter
import com.example.frogmidevtest.Presenter.RoutePresenter

import com.example.frogmidevtest.R
import com.example.frogmidevtest.View.Adapters.RouteListAdapter
import io.realm.RealmResults
import org.json.JSONObject

/**
*   This fragment contains the RoutesList
 */
class RouteFragment : Fragment(), Interfaces.RequiredViewMethods,Interfaces.RouteContract.View{
    companion object {
        // We Receive 2 params from the StopFragment, the InternetStatus and the stop_id that we use to make the Query.
        @JvmStatic
        fun newInstance(stop_id:String,mInternetStatus:Boolean): RouteFragment{
            return RouteFragment().apply {
                arguments = Bundle().apply {
                    putString("stop_id", stop_id)
                    putBoolean("mInternetStatus", mInternetStatus)
                }
            }
        }
    }

    var mInternetStatus : Boolean = true
    lateinit var mPresenter : Interfaces.ProvidedPresenterMethods
    lateinit var adapter: RouteListAdapter
    var mListStops = ArrayList<RouteDataModel>()
    lateinit var mStopId :String


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_routes, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            mStopId = it.getString("stop_id").toString()
            mInternetStatus = it.getBoolean("mInternetStatus")
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val activity = activity as Context
        val recyclerView = view!!.findViewById<RecyclerView>(R.id.recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        adapter = RouteListAdapter(mListStops)
        recyclerView.adapter = adapter
        setupMVP()

    }

    override fun getAppContext(): Context {
        return this.context!!
    }

    override fun getActivityContext(): Context {
        return getActivityContext()
    }
    override fun notifyStopsChanged(stopList: ArrayList<Results>) {/*NOT APPLY*/}

    override fun notifyRouteChanged(routeList: ArrayList<RouteDataModel>) {
        mListStops.clear()
        mListStops.addAll(routeList)
        adapter.notifyDataSetChanged()
    }

    private fun setupMVP(){
        activity!!.title = "paradero $mStopId"
        val presenter = Presenter(this)
        val model = Model(presenter)
        presenter.setModel(model)
        Presenter(this)
        mPresenter = presenter
        mOfflinePresenter = RoutePresenter(this)
        //If we have internet we ask for Routes with Volley, if dont we ask for Routes in Realm
        if(mInternetStatus) mPresenter.updateRoutesValues(mStopId)
        if(!mInternetStatus) mOfflinePresenter.loadRoutes(this,mStopId)
    }

    override fun notifyAddedChanged(stopList: ArrayList<Results>) {/*NOT APPLY*/}

    /**
     *  Offline Implementation
     *
     * */

    override fun onRouteLoaded(realmResults: RealmResults<Route>?) {
        realmResults?.let {
            mListStops.clear()
            for (results in it) {
                Log.e("INFODB", results.direction)
                Log.e("INFODB", results.route_long_name)
                Log.e("INFODB", results.agency_id)
                Log.e("INFODB", results.route_id)
                Log.e("INFODB", results.stop_id)

                val mDirection = JSONObject(results.direction)
                val mStopDataModel = RouteDataModel(
                    results.route_long_name,
                    results.route_type,
                    results.route_text_color,
                    results.agency_id,
                    results.route_id,
                    results.route_color,
                    Direction(
                        mDirection.getString("direction_id"),
                        mDirection.getString("direction_headsign"),
                        mDirection.getString("direction_name"),
                        mDirection.getString("route_id")
                    )
                )
                mListStops.add(mStopDataModel)
            }
            adapter.notifyDataSetChanged()
        }
    }

    override fun getViewModel(): RouteViewModel {
        return mRouteViewModel
    }

    override lateinit var mOfflinePresenter: Interfaces.RouteContract.Presenter
    private val mRouteViewModel: RouteViewModel by lazy {
        ViewModelProviders.of(this).get(RouteViewModel::class.java)
    }

    override fun onError(errorMessage: Int) {
        Log.e("error", "errorMessage$errorMessage")
    }

    override fun onDataLoading() {
        Log.d("INFO","WORKING---------")
    }

}
