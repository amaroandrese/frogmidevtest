package com.example.frogmidevtest.View.Adapters

import android.graphics.Color
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.frogmidevtest.Model.DataModel.RouteDataModel
import com.example.frogmidevtest.R
import kotlinx.android.synthetic.main.recycler_item_route.view.*

/**
 * RouteViewHolder: Is the ViewHolder extension for the RoutesListAdapter
 * */

class RouteViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

    fun bind(mRouteDataModel: RouteDataModel) = with(itemView) {

        val mRouteId = mRouteDataModel.route_id
        val mRouteName = mRouteDataModel.route_long_name
        val mDirection = mRouteDataModel.direction.direction_headsign

        itemView.routeName.text = mRouteName
        itemView.routeCode.text = mRouteId
        itemView.routeHeadSing.text = "Dirección: $mDirection"
        itemView.routeImage.setImageDrawable(ContextCompat.getDrawable(itemView.context, R.drawable.stopimage))
        var mItemViewColor = mRouteDataModel.route_color
        itemView.routelinearLayout.setBackgroundColor(Color.parseColor("#$mItemViewColor"))
        itemView.routecodelinearLayout.setBackgroundColor(Color.parseColor("#$mItemViewColor"))

    }
}