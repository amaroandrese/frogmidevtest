package com.example.frogmidevtest.View.Adapters

import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.frogmidevtest.Dao.RealmObject.Results
import com.example.frogmidevtest.R
import kotlinx.android.synthetic.main.recycler_item_stop.view.*

/**
 * StopViewHolder: Is the ViewHolder extension for the StopListAdapter
 * */
class StopViewHolder(
    itemView: View): RecyclerView.ViewHolder(itemView){
    fun bind(mStopDataModel: Results, clickListener: StopListAdapter.ItemClickListener) = with(itemView) {

        val mStopCode = "Código del Paradero: "+mStopDataModel.stop_code
        val mStopName = mStopDataModel.stop_name
        itemView.stopName.text = mStopName
        itemView.stopCode.text = mStopCode
        itemView.stopImage.setImageDrawable(ContextCompat.getDrawable(itemView.context, R.drawable.busstop))
        itemView.setOnClickListener{clickListener.onCLick(mStopDataModel)
        }
    }
}