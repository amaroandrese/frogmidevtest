package com.example.frogmidevtest.View

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.frogmidevtest.Dao.RealmObject.Results
import com.example.frogmidevtest.Dao.RealmObject.Stop
import com.example.frogmidevtest.ViewModel.StopViewModel
import com.example.frogmidevtest.InetUtils
import com.example.frogmidevtest.Interfaces
import com.example.frogmidevtest.Model.DataModel.RouteDataModel
import com.example.frogmidevtest.Model.Model
import com.example.frogmidevtest.Presenter.Presenter
import com.example.frogmidevtest.Presenter.StopPresenter
import com.example.frogmidevtest.R
import com.example.frogmidevtest.View.Adapters.StopListAdapter
import io.realm.RealmResults
import org.json.JSONArray

/**
 * StopFragment : This fragment contains the Stops List.
 */

class StopFragment : Fragment(),Interfaces.RequiredViewMethods,StopListAdapter.ItemClickListener,Interfaces.StopContract.View{
    companion object {
        @JvmStatic
        fun newInstance(): StopFragment{
            return StopFragment()
        }
    }

    override lateinit var mOfflinePresenter: Interfaces.StopContract.Presenter
    lateinit var mPresenter : Interfaces.ProvidedPresenterMethods
    lateinit var adapter: StopListAdapter
    var mListStops = ArrayList<Results>()
    var pageSwipes:Int = 1
    lateinit var swipeContainer:SwipeRefreshLayout
    private var loading = true
    var pastVisiblesItems = 0
    var visibleItemCount:Int = 0
    var totalItemCount:Int = 0
    var mInternetStatus : Boolean = true



    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val activity = activity as Context
        val recyclerView = view!!.findViewById<RecyclerView>(R.id.recycler_view)
        val mLayoutManager: LinearLayoutManager
        mLayoutManager = LinearLayoutManager(activity)
        recyclerView.layoutManager = mLayoutManager
        swipeContainer = view!!.findViewById(R.id.swipeContainer)
        swipeContainer.setOnRefreshListener {
            swipeContainer.isRefreshing = false
        }
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0){
                    visibleItemCount = mLayoutManager.childCount
                    totalItemCount = mLayoutManager.itemCount
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition()
                    if (loading && mInternetStatus) {
                        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
                            loading = false
                            mPresenter.addStopsValues(pageSwipes+1)
                            swipeContainer.isRefreshing = true
                        }
                    }
                }
            }
        })
        adapter = StopListAdapter(mListStops,this)
        recyclerView.adapter = adapter
        setupMVP()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_stops, container, false)

    }

    override fun getAppContext(): Context {
        return this.context!!
    }

    override fun getActivityContext(): Context {
        return getActivityContext()
    }

    override fun notifyStopsChanged(stopList: ArrayList<Results>) {
        mListStops.clear()
        mListStops.addAll(stopList)
        adapter.notifyDataSetChanged()

    }

    override fun notifyAddedChanged(stopList: ArrayList<Results>) {
        if(stopList.size>0){
            mListStops.addAll(mListStops.size,stopList)
            adapter.notifyDataSetChanged()
            loading = true
            swipeContainer.isRefreshing = false
            pageSwipes++
        }else{
            loading = true
            swipeContainer.isRefreshing = false
        }

    }

    override fun notifyRouteChanged(routeList: ArrayList<RouteDataModel>) {/*NOT APPLY*/}

    override fun onCLick(mStopDataModel: Results) {
        val mRoutesFragment = RouteFragment.newInstance(mStopDataModel.stop_id,mInternetStatus)
        requireActivity().supportFragmentManager.beginTransaction().replace(R.id.root_layout,mRoutesFragment,"RouteFragment").addToBackStack(null).commit()
    }

    private fun setupMVP(){
        activity!!.title = "Paraderos"
        val presenter = Presenter(this)
        val model = Model(presenter)
        presenter.setModel(model)
        Presenter(this)
        mPresenter = presenter
        mOfflinePresenter = StopPresenter(this)
        checkInternet()
        if(mInternetStatus && mListStops.size==0) mPresenter.updateStopsValues(1)
        if(!mInternetStatus) mOfflinePresenter.loadStop(this,1)
    }

/**
 *  Offline Implementation
 *
 * */


    override fun onError(errorMessage: Int) {
        Log.e("error", "errorMessage$errorMessage")
    }

    override fun onDataLoading() {
        Log.d("INFO","WORKING---------")
    }

    private val mStopViewModel: StopViewModel by lazy {
        ViewModelProviders.of(this).get(StopViewModel::class.java)
    }

    //offline fun to load data from Realm
    override fun onStopLoaded(realmResults: RealmResults<Stop>?) {
        realmResults?.let {
            mListStops.clear()
            for (results in it) {
                val mStops = JSONArray(results.results)
                for (i in 0 until mStops.length()){
                    val mResults = mStops.getJSONObject(i)
                    var mStopDataModel =
                        Results(
                            mResults.getString("stop_lat"),
                            mResults.getString("stop_code"),
                            mResults.getString("stop_lon"),
                            mResults.getString("agency_id"),
                            mResults.getString("stop_id"),
                            mResults.getString("stop_name")
                        )
                    mListStops.add(mStopDataModel)
                }
            }
            adapter.notifyDataSetChanged()
        }
    }

    override fun getViewModel(): StopViewModel {
        return mStopViewModel
    }

    private fun checkInternet(){
        val loader = Thread {
            when {
                InetUtils.hasInternetConnected(context!!) -> {
                    mInternetStatus = true
                }
                else -> {
                    mInternetStatus = false
                }
            }
        }
        loader.start()
    }
}
