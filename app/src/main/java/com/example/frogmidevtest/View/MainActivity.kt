package com.example.frogmidevtest.View

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.frogmidevtest.R

/**
 * We Use MainActivity to launch the StopFragment
 * */

class MainActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.root_layout, StopFragment.newInstance(), "stopList")
                .commit()
        }
    }
}
