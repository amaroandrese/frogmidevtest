package com.example.frogmidevtest

import android.content.Context
import android.net.ConnectivityManager
import android.provider.SyncStateContract
import android.util.Log
import android.util.Log.e
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL
import java.util.logging.Logger


/**
 * Object that has the functions to ping a URL to see if the device has internet.
 * */

object InetUtils {

    var classTag = "InetUtils"

    private fun hasNetworkAvailable(context: Context): Boolean {
        val service = Context.CONNECTIVITY_SERVICE
        val manager = context.getSystemService(service) as ConnectivityManager?
        val network = manager?.activeNetworkInfo
        Log.d(classTag, "hasNetworkAvailable: ${(network != null)}")
        return (network != null)
    }

    //Constants.REACHABILITY_SERVER = "https://www.google.com"
    fun hasInternetConnected(context: Context): Boolean {
        if (hasNetworkAvailable(context)) {
            try {
                val connection = URL("https://www.google.com").openConnection() as HttpURLConnection
                connection.setRequestProperty("User-Agent", "ConnectionTest")
                connection.setRequestProperty("Connection", "close")
                connection.connectTimeout = 1000 // configurable
                connection.connect()
                Log.d(classTag, "hasInternetConnected: ${(connection.responseCode == 200)}")
                return (connection.responseCode == 200)
            } catch (e: IOException) {
                Log.e(classTag, "Error checking internet connection", e)
            }
        } else {
            Log.w(classTag, "No network available!")
        }
        Log.d(classTag, "hasInternetConnected: false")
        return false
    }

}